# README

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

Author: Thomas Kismarton, tkismar2@uoregon.edu
UO ID: 951898095
Functionality: A basic webserver that displays webpages present in the 'pages' directory. Displays the appropriate errors
               when missing or forbidden webpages try to be accessed.
